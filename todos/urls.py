from django.contrib import admin
from django.urls import path

from .views import todo_list_list

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", todo_list_list, name="todo_list_list"),
]
